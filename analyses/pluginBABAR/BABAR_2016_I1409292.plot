BEGIN PLOT /BABAR_2016_I1409292/d01-x01-y01
Title=$K^+\pi^+\pi^-$ mass  distribution in $B^+\to K^+\pi^+\pi^-\gamma$
XLabel=$m_{K^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2016_I1409292/d02-x01-y01
Title=$K^+\pi^-$ mass  distribution in $B^+\to K^+\pi^+\pi^-\gamma$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
