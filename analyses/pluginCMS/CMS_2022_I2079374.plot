BEGIN PLOT /CMS_2022_I2079374/d01-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $50<m_{\ell\ell}<76$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d03-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $76<m_{\ell\ell}<106$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d05-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $106<m_{\ell\ell}<170$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d07-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $170<m_{\ell\ell}<350$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d09-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $350<m_{\ell\ell}<1000$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.3
END PLOT



BEGIN PLOT /CMS_2022_I2079374/d11-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^*(\to \ell^+ \ell^-)+\text{jet}$, $50<m_{\ell\ell}<76$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.5
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d13-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^*(\to \ell^+ \ell^-)+\text{jet}$, $76<m_{\ell\ell}<106$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.5
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d15-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^*(\to \ell^+ \ell^-)+\text{jet}$, $106<m_{\ell\ell}<170$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.5
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d17-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^*(\to \ell^+ \ell^-)+\text{jet}$, $170<m_{\ell\ell}<350$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{\mathrm{T}}(\ell\ell)$ [pb/GeV]
LogX=1
LogY=1
XMin=0.5
END PLOT



BEGIN PLOT /CMS_2022_I2079374/d19-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $50<m_{\ell\ell}<76$ GeV
XLabel=$\phi^*_\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\phi^*_\eta$ [pb]
LogX=1
LogY=1
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d21-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $76<m_{\ell\ell}<106$ GeV
XLabel=$\phi^*_\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\phi^*_\eta$ [pb]
LogX=1
LogY=1
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d23-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $106<m_{\ell\ell}<170$ GeV
XLabel=$\phi^*_\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\phi^*_\eta$ [pb]
LogX=1
LogY=1
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d25-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $170<m_{\ell\ell}<350$ GeV
XLabel=$\phi^*_\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\phi^*_\eta$ [pb]
LogX=1
LogY=1
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d27-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $350<m_{\ell\ell}<1000$ GeV
XLabel=$\phi^*_\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\phi^*_\eta$ [pb]
LogX=1
LogY=1
XMin=0.001
END PLOT




BEGIN PLOT /CMS_2022_I2079374/d29-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$,  $50<m_{\ell\ell}<76$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LogX=1
LogY=0
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d31-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$,  $106<m_{\ell\ell}<170$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d33-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $170<m_{\ell\ell}<350$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.3
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d35-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $350<m_{\ell\ell}<1000$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.3
END PLOT



BEGIN PLOT /CMS_2022_I2079374/d37-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $50<m_{\ell\ell}<76$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LogX=1
LogY=0
XMin=0.5
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d39-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $106<m_{\ell\ell}<170$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.5
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d41-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $170<m_{\ell\ell}<350$ GeV
XLabel=$p_{\mathrm{T}}(\ell\ell)$ [GeV]
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.5
END PLOT



BEGIN PLOT /CMS_2022_I2079374/d43-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $50<m_{\ell\ell}<76$ GeV
XLabel=$\phi^*_\eta$
YLabel=Ratio to Z peak region
LegendXPos=0.05
LogX=1
LogY=0
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d45-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $106<m_{\ell\ell}<170$ GeV
XLabel=$\phi^*_\eta$
YLabel=Ratio to Z peak region
LogX=1
LogY=0
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d47-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $170<m_{\ell\ell}<350$ GeV
XLabel=$\phi^*_\eta$
YLabel=Ratio to Z peak region
LogX=1
LogY=0
XMin=0.001
END PLOT

BEGIN PLOT /CMS_2022_I2079374/d49-x01-y01
Title=CMS, 13 TeV, $\mathrm{Z}/\gamma^* \to \ell^+ \ell^-$, $350<m_{\ell\ell}<1000$ GeV
XLabel=$\phi^*_\eta$
YLabel=Ratio to Z peak region
LogX=1
LogY=0
XMin=0.001
END PLOT
