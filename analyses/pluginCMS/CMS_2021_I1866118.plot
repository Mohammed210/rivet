BEGIN PLOT /CMS_2021_I1866118/d0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d01-x01-y01
XLabel=$\Delta\phi (Z,j1)$
YLabel=$\frac{d\sigma}{d(\Delta\phi^{Z,j1})}$
Title=CMS, 13 TeV, Z$+\geq 1$ jet
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d06-x01-y01
XLabel=$\Delta\phi (Z,j1)$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{d(\Delta\phi^{Z,j1})}$
Title=CMS, 13 TeV, Z$+\geq 1$ jet
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d02-x01-y01
XLabel=$\Delta_{rel} p_{T} (Z,j1)$
YLabel=$\frac{d\sigma}{d(\Delta_{rel} p_{T}^{Z,j1})}$
Title=CMS, 13 TeV, Z$+\geq 1$ jet
LegendXPos=0.5
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d07-x01-y01
XLabel=$\Delta_{rel} p_{T} (Z,j1)$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{d(\Delta_{rel} p_{T}^{Z,j1})}$
Title=CMS, 13 TeV, Z$+\geq 1$ jet
LegendXPos=0.5
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d03-x01-y01
XLabel=$\Delta\phi (Z,dijet)$
YLabel=$\frac{d\sigma}{d(\Delta\phi^{Z,dijet})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d08-x01-y01
XLabel=$\Delta\phi (Z,dijet)$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{d(\Delta\phi^{Z,dijet})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d04-x01-y01
XLabel=$\Delta_{rel} p_{T} (Z,dijet)$
YLabel=$\frac{d\sigma}{d(\Delta_{rel} p_{T}^{Z,dijet})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.5
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d09-x01-y01
XLabel=$\Delta_{rel} p_{T} (Z,dijet)$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{d(\Delta_{rel} p_{T}^{Z,dijet})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.5
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d05-x01-y01
XLabel=$\Delta_{rel} p_{T} (j1,j2)$
YLabel=$\frac{d\sigma}{d(\Delta_{rel} p_{T}^{j1,j2})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /CMS_2021_I1866118/d10-x01-y01
XLabel=$\Delta_{rel} p_{T} (j1,j2)$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{d(\Delta_{rel} p_{T}^{j1,j2})}$
Title=CMS, 13 TeV, Z$+\geq 2$ jets
LegendXPos=0.05
# END PLOT
