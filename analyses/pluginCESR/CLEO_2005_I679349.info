Name: CLEO_2005_I679349
Year: 2005
Summary: Dalitz plot analysis of $D^0\to \pi^+\pi^-\pi^0$
Experiment: CLEO
Collider: CESR
InspireID: 679349
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 72 (2005) 031102
RunInfo: Any process producing D0 -> pi+pi-pi0, original e+e->Upsilon(4S)
Description:
  'Measurement of the mass distributions in the decay $D^0\to \pi^+\pi^-\pi^0$. The data were read from the plots in the paper.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2005uoz
BibTeX: '@article{CLEO:2005uoz,
    author = "Cronin-Hennessy, D. and others",
    collaboration = "CLEO",
    title = "{Searches for CP violation and pi pi S-wave in the Dalitz-Plot of D0 ---\ensuremath{>} pi+ pi- pi0}",
    eprint = "hep-ex/0503052",
    archivePrefix = "arXiv",
    reportNumber = "CLNS-05-1916, CLEO-05-8, CLEO-5-8",
    doi = "10.1103/PhysRevD.75.119904",
    journal = "Phys. Rev. D",
    volume = "72",
    pages = "031102",
    year = "2005",
    note = "[Erratum: Phys.Rev.D 75, 119904 (2007)]"
}
'
