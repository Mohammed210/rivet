BEGIN PLOT /CLEO_2007_I732065/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d01-x01-y02
Title=$\eta\pi^+$ mass distribution in $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m^2_{\eta\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d01-x01-y03
Title=$\eta\pi^-$ mass distribution in $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m^2_{\eta\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/dalitz_1
Title=Dalitz plot for $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m^2_{\eta\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^+}/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2007_I732065/d02-x01-y01
Title=$K^-\pi^0$ mass distribution in $\chi_{c1}\to K^+K^-\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d02-x01-y02
Title=$K^+\pi^0$ mass distribution in $\chi_{c1}\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d02-x01-y03
Title=$K^+K^-$ mass distribution in $\chi_{c1}\to K^+K^-\pi^0$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/dalitz_2
Title=Dalitz plot for $\chi_{c1}\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^0}/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d03-x01-y01
Title=$\pi^\pm K^\mp$ mass distribution in $\chi_{c1}\to K^\mp\pi^\pm K^0_S$
XLabel=$m^2_{\pi^\pm K^\mp}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^\pm K^\mp}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d03-x01-y02
Title=$\pi^\pm K^0_S$ mass distribution in $\chi_{c1}\to K^\mp\pi^\pm K^0_S$
XLabel=$m^2_{\pi^\pm K^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^\pm K^0_S}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/d03-x01-y03
Title=$K^\mpK^0_S$ mass distribution in $\chi_{c1}\to K^\mp\pi^\pm K^0_S$
XLabel=$m^2_{K^\mpK^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^\mpK^0_S}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2007_I732065/dalitz_3
Title=Dalitz plot for $\chi_{c1}\to K^\mp\pi^\pm K^0_S$
XLabel=$m^2_{\pi^\pm K^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^\pm K^\mp}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^\pm K^0_S}/\mathrm{d}m^2_{\pi^\pm K^\mp}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT
