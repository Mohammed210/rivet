Name: CLEO_2013_I1081165
Year: 2013
Summary: Kinematic distributions in $D^0\to \pi^-\pi^0 e^+\nu_e$ and $D^+\to \pi^+\pi^- e^+\nu_e$ 
Experiment: CLEO
Collider: CESR
InspireID: 1081165
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 110 (2013) 13, 131802
RunInfo: Any process producing D0 or D+
Description:
  'Measurement of the kinematic distributions in $D^0\to \pi^-\pi^0 e^+\nu_e$ and $D^+\to \pi^+\pi^- e^+\nu_e$ by CLEO. N.B. the plots where read from the paper and may not have been corrected for acceptance.'
ValidationInfo:
  'Herwig 7 events using EvtGen for the decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2011ab
BibTeX: '@article{CLEO:2011ab,
    author = "Dobbs, S. and others",
    collaboration = "CLEO",
    title = "{First Measurement of the Form Factors in the Decays $D^0 \to \rho^- e^+ \nu_e$ and $D^+ \to \rho^0 e^+ \nu_e$}",
    eprint = "1112.2884",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CLNS-11-2075, CLEO-11-3",
    doi = "10.1103/PhysRevLett.110.131802",
    journal = "Phys. Rev. Lett.",
    volume = "110",
    number = "13",
    pages = "131802",
    year = "2013"
}
'
