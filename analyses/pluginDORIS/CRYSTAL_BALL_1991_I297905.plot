BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d01-x01-y01
Title=$\pi^0$ multiplicity in continuum (10 GeV)
XLabel=
YLabel=$N_{\pi^0}$
LogY=0
LegendYPos=0.2
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d01-x01-y02
Title=$\eta$ multiplicity in continuum (10 GeV)
XLabel=
YLabel=$N_{\eta}$
LogY=0
LegendYPos=0.2
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d02-x01-y01
Title=$\pi^0$ multiplicity in $\Upsilon(1S)$ decays
XLabel=
YLabel=$N_{\pi^0}$
LogY=0
LegendYPos=0.2
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d02-x01-y02
Title=$\eta$ multiplicity in $\Upsilon(1S)$ decays
XLabel=
YLabel=$N_{\eta}$
LogY=0
LegendYPos=0.2
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d03-x01-y01
Title=$\pi^0$ spectrum in the continuum (10 GeV)
XLabel=$z$
YLabel=$\frac{1}{\beta N_{\mathrm{hadrons}}} \mathrm{d}N_{\pi^0}/\mathrm{d}z$
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d03-x01-y02
Title=$\pi^0$ spectrum in the continuum (10 GeV)
XLabel=$z$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d04-x01-y01
Title=$\pi^0$ spectrum in $\Upsilon(1S)$ decays
XLabel=$z$
YLabel=$\frac{1}{\beta N_{\mathrm{hadrons}}} \mathrm{d}N_{\pi^0}/\mathrm{d}z$
LegendXPos=0.7
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d05-x01-y01
Title=$\eta$ spectrum in the continuum (10 GeV)
XLabel=$z$
YLabel=$\frac{1}{\beta N_{\mathrm{hadrons}}} \mathrm{d}N_{\eta}/\mathrm{d}z$
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d05-x01-y02
Title=$\eta$ spectrum in the continuum (10 GeV)
XLabel=$z$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I297905/d06-x01-y01
Title=$\eta$ spectrum in $\Upsilon(1S)$ decays
XLabel=$z$
YLabel=$\frac{1}{\beta N_{\mathrm{hadrons}}} \mathrm{d}N_{\eta}/\mathrm{d}z$
LegendXPos=0.7
END PLOT
