Name: ARGUS_1987_I248655
Year: 1987
Summary: Direct Photon Spectrum in $\Upsilon(1S)$ decays
Experiment: ARGUS
Collider: DORIS
InspireID: 248655
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 199 (1987) 291-296
RunInfo: Any process producing Upsilon mesons, originally e+e- collisions
Description:
  'Measurement of the direct photon spectrum in $\Upsilon(1S)$ decays by the ARGUS experiment. While there is a more recent CLEO result that is not corrected for efficiency and resolution. The spectrum was read from the figures in the paper as it was not included in the original HEPDATA entry. The spectrum is not corrected for resolution and therefore the photon energies are smeared using the resolution given in the paper.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: ARGUS:1987ujr
BibTeX: '@article{ARGUS:1987ujr,
    author = "Albrecht, H. and others",
    collaboration = "ARGUS",
    title = "{Determination of $\alpha^- s$ From a Measurement of the Direct Photon Spectrum in $\Upsilon$ (1s) Decays}",
    reportNumber = "DESY-87-087",
    doi = "10.1016/0370-2693(87)91377-3",
    journal = "Phys. Lett. B",
    volume = "199",
    pages = "291--296",
    year = "1987"
}
'
