BEGIN PLOT /BELLE_2021_I1859517/d01-x01-y01
Title=$\cos\theta$ for $\Xi^0_c\to\Lambda^0\bar{K}^{*0}$ and $\Lambda^0\to p^+\pi^-$ 
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1859517/d02-x01-y01
Title=$\cos\theta$ for $\Xi^0_c\to\Sigma^0\bar{K}^{*0}$ and $\Sigma^0\to \Lambda^0\gamma$ 
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1859517/d03-x01-y01
Title=$\cos\theta$ for $\Xi^0_c\to\Sigma^+\bar{K}^{*-}$ and $\Sigma^+\to p^+\pi^0$ 
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1859517/d04-x01-y01
Title=$\alpha$ for $\Xi^0_c\to\Lambda^0\bar{K}^{*0}$ and $\Lambda^0\to p^+\pi^-$
YLabel=$\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1859517/d04-x01-y02
Title=$\alpha$ for $\Xi^0_c\to\Sigma^0\bar{K}^{*0}$ and $\Sigma^0\to \Lambda^0\gamma$ 
YLabel=$\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1859517/d04-x01-y03
Title=$\alpha$ for $\Xi^0_c\to\Sigma^+\bar{K}^{*-}$ and $\Sigma^+\to p^+\pi^0$ 
YLabel=$\alpha$
LogY=0
END PLOT
