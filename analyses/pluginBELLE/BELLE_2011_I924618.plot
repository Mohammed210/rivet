BEGIN PLOT /BELLE_2011_I924618/d01-x01-y01
Title=Differential branching ratio for $B^-\to\bar{p}\Lambda^0D^0$
XLabel=$m_{\bar{p}\Lambda^0}$ [GeV]
YLabel=$\text{d}\mathrm{B}/\text{D}m_{\bar{p}\Lambda^0}$ [$10^{-6}/\text{GeV}$]
LogY=0
END PLOT
