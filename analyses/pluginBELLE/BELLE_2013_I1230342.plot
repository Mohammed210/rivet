BEGIN PLOT /BELLE_2013_I1230342/d01-x01-y01
Title=Differential branching ratio w.r.t $\Lambda_c^+\bar{\Lambda}^0$ mass for $\bar{B}^0_s\to \Lambda_c^+\bar{\Lambda}^0\pi^-$
XLabel=$m_{\Lambda_c^+\bar{\Lambda}^0}$ [GeV]
YLabel=$\text{d}\mathrm{B}/\text{D}m_{\Lambda_c^+\bar{\Lambda}^0}$ [$10^{-4}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1230342/d01-x01-y02
Title=Differential branching ratio w.r.t $\Lambda_c^+\pi^-$ mass for $\bar{B}^0_s\to \Lambda_c^+\bar{\Lambda}^0\pi^-$
XLabel=$m_{\Lambda_c^+\pi^-}$ [GeV]
YLabel=$\text{d}\mathrm{B}/\text{D}m_{\Lambda_c^+\pi^-}$ [$10^{-4}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1230342/d01-x01-y03
Title=Differential branching ratio w.r.t $\bar{\Lambda}^0\pi^-$ mass for $\bar{B}^0_s\to \Lambda_c^+\bar{\Lambda}^0\pi^-$
XLabel=$m_{\bar{\Lambda}^0\pi^-}$ [GeV]
YLabel=$\text{d}\mathrm{B}/\text{D}m_{\bar{\Lambda}^0\pi^-}$ [$10^{-4}/\text{GeV}$]
LogY=0
END PLOT
