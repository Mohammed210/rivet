BEGIN PLOT /BELLE_2022_I2160445/d01-x01-y01
Title=$pK^0_S$ mass distribution in $\Lambda_c^+\to p K^0_SK^0_S$
XLabel=$m^2_{pK^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{pK^0_S}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2160445/d01-x01-y02
Title=$K^0_SK^0_S$ mass distribution in $\Lambda_c^+\to p K^0_SK^0_S$
XLabel=$m^2_{K^0_SK^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^0_S}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2022_I2160445/d02-x01-y01
Title=$K^0_S\eta$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m^2_{K^0_S\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2160445/d02-x01-y02
Title=$pK^0_S$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m^2_{pK^0_S}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{pK^0_S}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2160445/d02-x01-y03
Title=$p\eta$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m^2_{p\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{p\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
