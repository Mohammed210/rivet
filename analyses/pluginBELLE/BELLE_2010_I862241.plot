BEGIN PLOT /BELLE_2010_I862241/d01-x01-y01
Title=$K^0_SK^+$ mass in $B^0\to K^0_SK^+K^-$
XLabel=$m_{K^0_SK^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I862241/d01-x01-y02
Title=$K^0_SK^-$ mass in $B^0\to K^0_SK^+K^-$
XLabel=$m_{K^0_SK^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I862241/d01-x01-y03
Title=$K^+K^-$ mass in $B^0\to K^0_SK^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I862241/d01-x01-y04
Title=$K^+K^-$ mass in $B^0\to K^0_SK^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
