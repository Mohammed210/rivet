BEGIN PLOT /TASSO_1984_I194774/d01-x01-y01
Title=Scaled energy for $D_s^\pm$ at 34.7 GeV
XLabel=$x_E$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
