BEGIN PLOT /LHCB_2020_I1787090/d01-x01-y01
Title=$B^0_s\to D_s^{*-} \mu^+\nu_\mu$
XLabel=$w$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}w$ 
LogY=0
END PLOT
