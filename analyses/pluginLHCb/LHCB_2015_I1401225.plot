BEGIN PLOT /LHCB_2015_I1401225/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+ \mu^+\mu^-$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{MeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{MeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1401225/d01-x01-y02
Title=$\mu^+\mu^-$ mass distribution in $D^0\to K^-\pi^+ \mu^+\mu^-$
XLabel=$m^2_{\mu^+\mu^-}$ [$\mathrm{MeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\mu^+\mu^-}$ [$\mathrm{MeV}^{-1}$]
LogY=0
END PLOT
