BEGIN PLOT /BESIII_2021_I1867196/d01-x01-y01
Title=$\sigma(e^+e^-\to D_s^{*+} D^*_{s0}(2317)^- +\text{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to  D_s^{*+} D^*_{s0}(2317)^- +\text{c.c})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1867196/d02-x01-y01
Title=$\sigma(e^+e^-\to D_s^{*+} D_{s1}(2460)^- +\text{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to  D_s^{*+} D_{s1}(2460)^- +\text{c.c})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1867196/d03-x01-y01
Title=$\sigma(e^+e^-\to D_s^{*+} D_{s1}(2536)^- +\text{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to  D_s^{*+} D_{s1}(2536)^- +\text{c.c})$/pb
ConnectGaps=1
END PLOT
