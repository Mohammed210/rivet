BEGIN PLOT /BESIII_2012_I1084539/d01-x01-y01
Title=$\cos\theta_{f_0(980)}$ distribution in $J/\psi\to\gamma\eta(1405)(\to f_0(980)\pi^0)$
XLabel=$\cos\theta_{f_0(980)}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{f_0(980)}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1084539/d01-x01-y02
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma\eta(1405)$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
