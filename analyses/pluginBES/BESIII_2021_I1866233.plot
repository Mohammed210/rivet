BEGIN PLOT /BESIII_2021_I1866233/d01-x01-y01
Title=$\sigma(e^+e^-\to \Xi^0\bar{\Xi}^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Xi^0\bar{\Xi}^0)$/pb
ConnectGaps=1
LogY=0
END PLOT
