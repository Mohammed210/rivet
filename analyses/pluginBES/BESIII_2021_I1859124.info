Name: BESIII_2021_I1859124
Year: 2021
Summary: Dalitz plot analysis of $D^+\to K^+ K^0_S\pi^0$
Experiment: BESIII
Collider: BEPC
InspireID: 1859124
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 104 (2021) 012006
RunInfo: Any process producing D+ -> K+ KS0 pi0
Description:
  'Measurement of the mass distributions in the decay $D^+\to K^+ K^0_S\pi^0$.
   The data were read from the plots in the paper.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021dmo
BibTeX: '@article{BESIII:2021dmo,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Study of the decay $D^+\to K^*(892)^+ K_S^0$ in $D^+\to K^+ K_S^0 \pi^0$}",
    eprint = "2104.09131",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.012006",
    journal = "Phys. Rev. D",
    volume = "104",
    pages = "012006",
    year = "2021"
}
'
