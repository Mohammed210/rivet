Name: BESIII_2022_I2051683
Year: 2022
Summary: Mass distributions in the decay $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
Experiment: BESIII
Collider: BEPC
InspireID: 2051683
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2203.06688 [hep-ex]
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022jts
BibTeX: '@article{BESIII:2022jts,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching fraction measurement of $D_{s}^{+} \to K^-K^+\pi^+\pi^+\pi^-$}",
    eprint = "2203.06688",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "3",
    year = "2022"
}
'
