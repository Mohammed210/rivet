BEGIN PLOT /BESII_2005_I690784/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\chi_{c0}\to \pi^+\pi^- K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2005_I690784/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $\chi_{c0}\to \pi^+\pi^- K^+K^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2005_I690784/d01-x01-y03
Title=$K\pi$ mass distribution in $\chi_{c0}\to \pi^+\pi^- K^+K^-$
XLabel=$m_{K\pi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K\pi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2005_I690784/d01-x01-y04
Title=$K\pi\pi$ mass distribution in $\chi_{c0}\to \pi^+\pi^- K^+K^-$
XLabel=$m_{K\pi\pi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K\pi\pi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
