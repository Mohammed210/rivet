Name: BESIII_2017_I1509920
Year: 2017
Summary:  $\psi(2S)\to e^+e^-\chi_{c(1,2)}$ and $\chi_{c(1,2)}\to e^+e^- J/\psi$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 1509920
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 118 (2017) 22, 221802
RunInfo: e+e- > psi 2s.
NeedCrossSection: no
Beams: [e-, e+]
Energies: [3.686]
Description:
  'Measurement of the $e^+e^-$ mass distribution and the helicity angle in the decays $\psi(2S)\to e^+e^-\chi_{c(1,2)}$ and $\chi_{c(1,2)}\to e^+e^- J/\psi$ decays.
   Data was read from the plots in the paper and the mass distributions are not corrected, although the angle distributions are'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2017ung
BibTeX: '@article{BESIII:2017ung,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Observation of $\psi(3686) \rightarrow e^{+}e^{-}\chi_{cJ}$ and $\chi_{cJ} \rightarrow e^{+}e^{-}J/\psi$}",
    eprint = "1701.05404",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.118.221802",
    journal = "Phys. Rev. Lett.",
    volume = "118",
    number = "22",
    pages = "221802",
    year = "2017"
}
'
