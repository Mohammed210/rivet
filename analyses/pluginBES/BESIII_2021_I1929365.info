Name: BESIII_2021_I1929365
Year: 2021
Summary: Dalitz plot analysis of $D_s^+\to \pi^+\pi^0\pi^0$
Experiment: BESIII
Collider:  BEPC
InspireID: 1929365
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 01 (2022) 052
Description:
  'Measurement of the mass distributions in the decay $D_s^+\to \pi^+\pi^0\pi^0$.
   The data were read from the plots in the paper.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021eru
BibTeX: '@article{BESIII:2021eru,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching fraction measurement of the decay $ {D}_s^{+} $ \textrightarrow{} \ensuremath{\pi}$^{+}$\ensuremath{\pi}$^{0}$\ensuremath{\pi}$^{0}$}",
    eprint = "2109.12660",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP01(2022)052",
    journal = "JHEP",
    volume = "01",
    pages = "052",
    year = "2022"
}
'
