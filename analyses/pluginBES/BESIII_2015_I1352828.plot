BEGIN PLOT /BESIII_2015_I1352828/d01-x01-y01
Title=$K^0_SK^\pm\pi^\mp$ mass distribution in $\chi_{c0}\to \phi K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1352828/d01-x01-y02
Title=$K^0_SK^\pm\pi^\mp$ mass distribution in $\chi_{c1}\to \phi K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1352828/d01-x01-y03
Title=$K^0_SK^\pm\pi^\mp$ mass distribution in $\chi_{c2}\to \phi K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1352828/d01-x02-y01
Title=$K^+K^-\pi^0$ mass distribution in $\chi_{c0}\to \phi K^+K^-\pi^0$
XLabel=$m_{K^+K^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1352828/d01-x02-y02
Title=$K^+K^-\pi^0$ mass distribution in $\chi_{c1}\to \phi K^+K^-\pi^0$
XLabel=$m_{K^+K^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1352828/d01-x02-y03
Title=$K^+K^-\pi^0$ mass distribution in $\chi_{c2}\to \phi K^+K^-\pi^0$
XLabel=$m_{K^+K^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
