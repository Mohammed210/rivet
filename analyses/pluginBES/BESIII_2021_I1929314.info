Name: BESIII_2021_I1929314
Year: 2021
Summary: Cross sections for $e^+e^-\to$ $K^+K^-\pi^+\pi^-(\pi^0)$, $K^+K^-K^+K^-(\pi^0)$, $\pi^+\pi^-\pi^+\pi^-(\pi^0)$ and $p\bar{p}\pi^+\pi^-(\pi^0)$ between 3.773 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1929314
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 104 (2021) 11, 112009
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams:  [e+,e-]
Description:
'Measurement of the dressed cross sections for $e^+e^-\to$ $K^+K^-\pi^+\pi^-(\pi^0)$, $K^+K^-K^+K^-(\pi^0)$, $\pi^+\pi^-\pi^+\pi^-(\pi^0)$ and $p\bar{p}\pi^+\pi^-(\pi^0)$
 between 3.773 and 4.6 GeV by the BESIII collaboration. There were three duplicate energy points in the data, for these points the one with higher statistics was retained.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021ftf
BibTeX: '@article{BESIII:2021ftf,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Cross sections for the reactions $e^+e^-\rightarrow K^+K^-\pi^+\pi^-(\pi^0)$, $K^+K^-K^+K^-(\pi^0)$, $\pi^+\pi^-\pi^+\pi^-(\pi^0)$, $p\bar{p}\pi^+\pi^-(\pi^0)$ in the energy region between 3.773 and 4.600 GeV}",
    eprint = "2109.12751",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.112009",
    journal = "Phys. Rev. D",
    volume = "104",
    number = "11",
    pages = "112009",
    year = "2021"
}
'
